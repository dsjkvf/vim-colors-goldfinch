# Goldfinch colorscheme for Vim

## About

Goldfinch is a (not so) minimal colorscheme for Vim editor somehow inspired by the famous [painting](https://en.wikipedia.org/wiki/The_Goldfinch_(painting)) by [Mauritshuis](https://en.wikipedia.org/wiki/Mauritshuis).

## Colors used

A few triplets were used -- here's a full listing according to the [chart](https://upload.wikimedia.org/wikipedia/en/1/15/Xterm_256color_chart.svg):

**White:**

255: Grey93  
250: Grey74  
248: Grey66  

**Black:**

016: Grey0  
236: Grey19  
240: Grey35  

**Blue:**

141: MediumPurple1              // not in the core  
111: SkyBlue2  
069: CornflowerBlue  

**Red:**

009: Red0  
088: DarkRed                    // not in the core  
124: Red3                       // not in the core  

**Gold:**

166: DarkOrange3                // not in the core  
208: DarkOrange  
214: Orange1  
221: LightGoldenrod2  
223: NavajoWhite1               // not in the core  
229: Wheat1  

## Installation

Copy `goldfinch.vim` to the `$VIMHOME/colors` directory, or simply use any plugin manager available.

## Screenshots

C:

![c](http://i.imgur.com/6aHjxKr.png)

Javascript:

![js](http://i.imgur.com/qnvkjGX.png)

Perl:

![perl](http://i.imgur.com/L3dSqi8.png)

Python:

![python](http://i.imgur.com/lbfY1It.png)

R:
![r](http://i.imgur.com/O9OgTem.png)

Diff:

![diff](http://i.imgur.com/RgeJdaH.png)

Markdown:

![MKD](http://i.imgur.com/WsoVNiz.png)

Vim:

![vim](http://i.imgur.com/FrQUbBb.png)
