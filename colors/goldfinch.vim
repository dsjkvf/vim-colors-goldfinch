" vim: set nowrap:

" HEADER
" Description: Vim color scheme, goldfinch.vim
" Author: dsjkvf (dsjkvf@gmail.com)
" Webpage: https://bitbucket.org/dsjkvf/vim-colors-goldfinch
" Notes: for `Normal` use either 229 or 221 in order to control the warmth, `%s/229/221/`

" INIT

" Options
hi clear
if exists("syntax_on")
    syntax reset
endif
let colors_name = "goldfinch"

" MAIN

" Vim
if &t_Co >= 256 || has("gui_running")

    " core
    hi Normal           cterm=NONE    ctermbg=016     ctermfg=229       gui=NONE    guibg=#000000     guifg=#ffffaf
    set background=dark

    " core interface
    hi ColorColumn      cterm=NONE    ctermbg=240     ctermfg=NONE      gui=NONE    guibg=#585858     guifg=NONE
    hi Conceal          cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi Cursor           cterm=NONE    ctermbg=fg      ctermfg=bg        gui=NONE    guibg=fg          guifg=bg
    hi CursorColumn     cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi CursorIM         cterm=NONE    ctermbg=fg      ctermfg=bg        gui=NONE    guibg=fg          guifg=bg
    hi CursorLine       cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi CursorLineNr     cterm=NONE    ctermbg=NONE    ctermfg=111       gui=NONE    guibg=NONE        guifg=#87afff
    hi DiffAdd          cterm=NONE    ctermbg=214     ctermfg=bg        gui=NONE    guibg=#ffaf00     guifg=bg
    hi DiffChange       cterm=NONE    ctermbg=fg      ctermfg=bg        gui=NONE    guibg=fg          guifg=bg
    hi DiffDelete       cterm=NONE    ctermbg=141     ctermfg=bg        gui=NONE    guibg=#af87ff     guifg=bg
    hi DiffText         cterm=NONE    ctermbg=214     ctermfg=bg        gui=NONE    guibg=#ffaf00     guifg=bg
    hi Directory        cterm=NONE    ctermbg=NONE    ctermfg=214       gui=NONE    guibg=NONE        guifg=#ffaf00
    hi ErrorMsg         cterm=bold    ctermbg=NONE    ctermfg=015       gui=bold    guibg=NONE        guifg=#ffffff
    hi FoldColumn       cterm=NONE    ctermbg=236     ctermfg=248       gui=NONE    guibg=#303030     guifg=#a8a8a8
    hi Folded           cterm=NONE    ctermbg=236     ctermfg=248       gui=NONE    guibg=#303030     guifg=#a8a8a8
    hi IncSearch        cterm=NONE    ctermbg=111     ctermfg=bg        gui=NONE    guibg=#87afff     guifg=bg
    hi LineNr           cterm=NONE    ctermbg=NONE    ctermfg=240       gui=NONE    guibg=NONE        guifg=#585858
    hi MatchParen       cterm=NONE    ctermbg=NONE    ctermfg=069       gui=NONE    guibg=NONE        guifg=#5f87ff
    hi ModeMsg          cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi MoreMsg          cterm=NONE    ctermbg=NONE    ctermfg=015       gui=NONE    guibg=NONE        guifg=#ffffff
    hi NonText          cterm=NONE    ctermbg=NONE    ctermfg=bg        gui=NONE    guibg=NONE        guifg=bg
    hi Pmenu            cterm=NONE    ctermbg=015     ctermfg=016       gui=NONE    guibg=#ffffff     guifg=#000000
    hi PmenuSbar        cterm=NONE    ctermbg=015     ctermfg=015       gui=NONE    guibg=#ffffff     guifg=#ffffff
    hi PmenuSel         cterm=bold    ctermbg=069     ctermfg=015       gui=bold    guibg=#5f87ff     guifg=#ffffff
    hi PmenuThumb       cterm=NONE    ctermbg=069     ctermfg=069       gui=NONE    guibg=#5f87ff     guifg=#5f87ff
    hi Question         cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi Search           cterm=NONE    ctermbg=069     ctermfg=bg        gui=NONE    guibg=#5f87ff     guifg=bg
    hi SignColumn       cterm=NONE    ctermbg=NONE    ctermfg=240       gui=NONE    guibg=NONE        guifg=#585858
    hi SpecialKey       cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi SpellBad         cterm=NONE    ctermbg=240     ctermfg=NONE      gui=NONE    guibg=#585858     guifg=NONE
    hi SpellCap         cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi SpellLocal       cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi SpellRare        cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi StatusLine       cterm=NONE    ctermbg=250     ctermfg=bg        gui=NONE    guibg=#bcbcbc     guifg=bg
    hi StatusLineNC     cterm=NONE    ctermbg=240     ctermfg=bg        gui=NONE    guibg=#585858     guifg=bg
    hi TabLine          cterm=NONE    ctermbg=240     ctermfg=bg        gui=NONE    guibg=#585858     guifg=bg
    hi TabLineFill      cterm=NONE    ctermbg=240     ctermfg=bg        gui=NONE    guibg=#585858     guifg=bg
    hi TabLineSel       cterm=NONE    ctermbg=250     ctermfg=bg        gui=NONE    guibg=#bcbcbc     guifg=bg
    hi Title            cterm=bold    ctermbg=NONE    ctermfg=009       gui=bold    guibg=NONE        guifg=#ff0000
    hi VertSplit        cterm=bold    ctermbg=NONE    ctermfg=250       gui=bold    guibg=NONE        guifg=#bcbcbc
    hi Visual           cterm=NONE    ctermbg=111     ctermfg=bg        gui=NONE    guibg=#87afff     guifg=bg
    hi VisualNOS        cterm=NONE    ctermbg=111     ctermfg=bg        gui=NONE    guibg=#87afff     guifg=bg
    hi WarningMsg       cterm=NONE    ctermbg=NONE    ctermfg=015       gui=NONE    guibg=NONE        guifg=#ffffff
    hi WildMenu         cterm=NONE    ctermbg=069     ctermfg=bg        gui=NONE    guibg=#5f87ff     guifg=bg

    " core development
    hi Comment          cterm=NONE    ctermbg=NONE    ctermfg=240       gui=NONE    guibg=NONE        guifg=#585858
    hi Constant         cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi Delimiter        cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi Error            cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi Identifier       cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi Ignore           cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi PreProc          cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi Special          cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi Statement        cterm=NONE    ctermbg=NONE    ctermfg=214       gui=NONE    guibg=NONE        guifg=#ffaf00
    hi String           cterm=NONE    ctermbg=NONE    ctermfg=221       gui=NONE    guibg=NONE        guifg=#ffd75f
    hi Todo             cterm=NONE    ctermbg=NONE    ctermfg=208       gui=NONE    guibg=NONE        guifg=#ff8700
    hi Type             cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi Underlined       cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE

    " for HTML
    hi htmlBold         cterm=NONE    ctermbg=NONE    ctermfg=208       gui=NONE    guibg=NONE        guifg=#ff8700
    hi htmlItalic       cterm=NONE    ctermbg=NONE    ctermfg=214       gui=NONE    guibg=NONE        guifg=#ffaf00
    hi htmlComment      cterm=NONE    ctermbg=NONE    ctermfg=240       gui=NONE    guibg=NONE        guifg=#585858
    hi htmlCommentPart  cterm=NONE    ctermbg=NONE    ctermfg=240       gui=NONE    guibg=NONE        guifg=#585858

    " for Markdown
    hi mkdString        cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi mkdCode          cterm=NONE    ctermbg=NONE    ctermfg=141       gui=NONE    guibg=NONE        guifg=#af87ff
    hi mkdCodeStart     cterm=NONE    ctermbg=NONE    ctermfg=147       gui=NONE    guibg=NONE        guifg=#afafff
    hi mkdCodeEnd       cterm=NONE    ctermbg=NONE    ctermfg=147       gui=NONE    guibg=NONE        guifg=#afafff
    hi mkdFootnote      cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi mkdBlockquote    cterm=NONE    ctermbg=NONE    ctermfg=223       gui=NONE    guibg=NONE        guifg=#ffd7af
    hi mkdComment       cterm=NONE    ctermbg=NONE    ctermfg=240       gui=NONE    guibg=NONE        guifg=#585858
    hi mkdListItem      cterm=NONE    ctermbg=NONE    ctermfg=214       gui=NONE    guibg=NONE        guifg=#ffaf00
    hi mkdRule          cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi mkdLineBreak     cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi mkdFootnotes     cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi mkdLink          cterm=NONE    ctermbg=NONE    ctermfg=111       gui=NONE    guibg=NONE        guifg=#87afff
    hi mkdURL           cterm=NONE    ctermbg=NONE    ctermfg=240       gui=NONE    guibg=NONE        guifg=#585858
    hi mkdInlineURL     cterm=NONE    ctermbg=NONE    ctermfg=111       gui=NONE    guibg=NONE        guifg=#87afff
    hi mkdID            cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi mkdLinkDef       cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi mkdLinkDefTarget cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi mkdLinkTitle     cterm=NONE    ctermbg=NONE    ctermfg=NONE      gui=NONE    guibg=NONE        guifg=NONE
    hi mkdDelimiter     cterm=NONE    ctermbg=NONE    ctermfg=240       gui=NONE    guibg=NONE        guifg=#585858

    " for Mail
    hi mailQuoted4      cterm=NONE    ctermbg=NONE    ctermfg=088       gui=NONE    guibg=NONE        guifg=#870000
    hi mailQuoted3      cterm=NONE    ctermbg=NONE    ctermfg=124       gui=NONE    guibg=NONE        guifg=#af0000
    hi mailQuoted2      cterm=NONE    ctermbg=NONE    ctermfg=166       gui=NONE    guibg=NONE        guifg=#d75f00
    hi mailQuoted1      cterm=NONE    ctermbg=NONE    ctermfg=214       gui=NONE    guibg=NONE        guifg=#ffaf00
    hi mailEmail        cterm=NONE    ctermbg=NONE    ctermfg=111       gui=NONE    guibg=NONE        guifg=#87afff
    hi mailURL          cterm=NONE    ctermbg=NONE    ctermfg=141       gui=NONE    guibg=NONE        guifg=#af87ff

    " for Vim
    hi vimCommentTitle  cterm=NONE    ctermbg=NONE    ctermfg=124       gui=NONE    guibg=NONE        guifg=#af0000

    " for Javascript
    hi javaScriptIdentifier cterm=NONE    ctermbg=NONE    ctermfg=214       gui=NONE    guibg=NONE        guifg=#ffaf00
    hi javaScriptFunction   cterm=NONE    ctermbg=NONE    ctermfg=214       gui=NONE    guibg=NONE        guifg=#ffaf00
    hi javaScriptType       cterm=NONE    ctermbg=NONE    ctermfg=214       gui=NONE    guibg=NONE        guifg=#ffaf00
    " pangloss/vim-javascript
    hi jsStorageClass   cterm=NONE    ctermbg=NONE    ctermfg=214       gui=NONE    guibg=NONE        guifg=#ffaf00
    hi jsFuncCall       cterm=NONE    ctermbg=NONE    ctermfg=214       gui=NONE    guibg=NONE        guifg=#ffaf00
    hi jsFunction       cterm=NONE    ctermbg=NONE    ctermfg=214       gui=NONE    guibg=NONE        guifg=#ffaf00

    " for Diff
    hi diffNewFile      cterm=NONE    ctermbg=NONE    ctermfg=141       gui=NONE    guibg=NONE        guifg=#af87ff
    hi diffFile         cterm=NONE    ctermbg=NONE    ctermfg=214       gui=NONE    guibg=NONE        guifg=#ffaf00
    hi diffRemoved      cterm=NONE    ctermbg=141     ctermfg=bg        gui=NONE    guibg=#af87ff     guifg=bg
    hi diffAdded        cterm=NONE    ctermbg=214     ctermfg=bg        gui=NONE    guibg=#ffaf00     guifg=bg
    hi diffLine         cterm=NONE    ctermbg=NONE    ctermfg=208       gui=NONE    guibg=NONE        guifg=#ff8700

" Else
else
    echoerr "This colorscheme is not supported by this terminal"
endif
